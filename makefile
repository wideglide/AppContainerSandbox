
TARGET = AppContainerSandbox.exe
SOURCE = Main.c ContainerCreate.c ContainerTest.c

AppContainerSandbox: $(TARGET)

$(TARGET): $(SOURCE)
    CL $** /D UNICODE /link /OUT:$@

clean:
	DEL *.obj *.exe

all: clean AppContainerSandbox