# AppContainer sandbox

Demonstrates some of the uses for an AppContainer.

 Originally from MalwareTech
 - https://www.malwaretech.com/2015/09/advanced-desktop-application-sandboxing.html
 - https://github.com/MalwareTech/AppContainerSandbox


## Example run

```
C:\Users\jb\source\AppContainerSandbox>AppContainerSandbox.exe
[Container Info]
name: MtSandboxTest
description: MalwareTech Sandbox Test
Sid: S-1-15-2-2765576060-2389096713-3079022642-4099546867-1445191215-3071396046-3987166893

Successfully executed AppContainerSandbox.exe in AppContainer
We are running in an app container

[+] Running filesystem test...
New path of %temp%: C:\Users\jb\AppData\Local\Packages\mtsandboxtest\AC\Temp
New path of %localappdata%: C:\Users\jb\AppData\Local\Packages\mtsandboxtest\AC
Opening of file C:\Users\jb\AppData\Local\Packages\mtsandboxtest\AC\Temp\allowed_test.txt was successful
Opening of file C:\Users\jb\desktop\allowed_test.txt was successful
Opening of file C:\Users\jb\desktop\blocked_test.txt returned access denied
[+] Filesystem testing done

[+] Running network test...
Connection to 74.125.227.200 was blocked
Connection to 10.19.89.5 was successful
[+] Network testing done

[+] Running process list testing...
Found process: [System Process]
Found process: AppContainerSandbox.exe
[+] Process list testing done

```
